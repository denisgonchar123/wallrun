// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include <Components/TimelineComponent.h>
#include "WallRunCharacter.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class USceneComponent;
class UCameraComponent;
class UMotionControllerComponent;
class UAnimMontage;
class USoundBase;

UENUM()
enum class EWallRunSide : int8
{
	Nome,
	Left,
	Right
};

UCLASS(config=Game)
class AWallRunCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
		//��� ��� ��� ����� �������� � ������ ������ 
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	USkeletalMeshComponent* Mesh1P;

	/** Gun mesh: 1st person view (seen only by self) */
		// ��������� ������  
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* FP_Gun;

	/** Location on gun mesh where projectiles should spawn. */
		//����� ������������ ����, ��� ������ �������� 
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USceneComponent* FP_MuzzleLocation;

	/** First person camera */
		//������ 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

	 
public:
	AWallRunCharacter();

	virtual void Tick(float DeltaSeconds) override;

protected:
	virtual void BeginPlay() override;

public:
	//�������� �������� ������ �����, ������ 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;
	
	//�������� �����, ���� 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;
	
	//��������� ������ �� ��������� 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector GunOffset;

	//����� ������� 
	UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class AWallRunProjectile> ProjectileClass;
	
	//���� �������� 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	USoundBase* FireSound;
	
	//�������� �������� 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	UAnimMontage* FireAnimation;

	virtual void Jump() override;

protected:
	void OnFire();
	void MoveForward(float Val);
	void MoveRight(float Val);

	//�������� �������� ������ �����, ������ 
	void TurnAtRate(float Rate);

	//������� ������ ����� ����
	void LookUpAtRate(float Rate);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WallRun", meta = (UIMin = 0.0f, ClampMin = 0.0f))
	float MaxWallRunTime = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WallRun")
	UCurveFloat* CameraTiltCurve; 

protected:
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	
public:
	USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

private:

	UFUNCTION()
	void OnPlayrCapsuleHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	void GetWallRunSideAndDirection(const FVector& HitNormal, EWallRunSide& OutSide, FVector& OutDirection) const;


	UFUNCTION()
	bool IsSurfaceWallRunable(const FVector& SurfaceNormal);

	bool AreRequiredKeysDown(EWallRunSide Side) const;

	float ForwardAxis = 0.0f;
	float RightAxis = 0.0f;

	void StartWallRun(EWallRunSide Side, const FVector& Direction);
	void StopWallRun();
	void UpdateWallRun();



	FORCEINLINE void BeginCameraTilt() { CameraTiltTimeline.Play(); }
	
	UFUNCTION()
	void UpdateCameraTilt(float Value);
	
	FORCEINLINE void EndCameraTilt() { CameraTiltTimeline.Reverse(); }

	bool bIsWallRuning = false;
	EWallRunSide CurrentWallRunSide = EWallRunSide::Nome;
	FVector CurrentWallRunDirection = FVector::ZeroVector;

	FTimerHandle WallRunTimer;
	FTimeline CameraTiltTimeline;


};

